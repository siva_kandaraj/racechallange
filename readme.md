# How to run

This solution is tested with vs 2017. 

To run the app, use vs2017 to build and run. 


# Things can be improved.

- Better handling of the data

    As of now the program either errors out silently or throws exceptions

- Test coverage

    Only part of the code has been covered

- Dependency injection

    Console app can use dependecy inject to some of the Interface to service class. 

- Exception handling

    Some silently ignored exceptions

# Time taken for this challange

It took little (30 mins) over than 2 hours, but I could not just stop without cleaning up the code :()