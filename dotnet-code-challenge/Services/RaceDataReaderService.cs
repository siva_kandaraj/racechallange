﻿using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge;
using RaceChallange.Models.Responses;

namespace RaceChallange.Services
{
    public class RaceDataReaderService
    {
        private readonly IEnumerable<IDataReadProvider> _dataProviders;

        public RaceDataReaderService(IEnumerable<IDataReadProvider> providers)
        {
            _dataProviders = providers;
        }

        public List<Horse> GetHorseList(string content)
        {
            var dataReadProvider = _dataProviders.FirstOrDefault(_ => _.CanRead(content));

            return dataReadProvider?.GetHorseList(content);
        }
    }
}
