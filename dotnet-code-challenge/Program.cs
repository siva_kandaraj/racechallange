﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using dotnet_code_challenge;
using RaceChallange.Providers;
using RaceChallange.Services;

namespace RaceChallange
{
    internal class Program
    {
        private static readonly IEnumerable<IDataReadProvider> Providers = new List<IDataReadProvider>
        {
            new XmlFeedReader(),
            new JsonFeedReader()
        };

        private static void Main(string[] args)
        {
            //TODO : Introduce logger

            var assembly = Assembly.GetExecutingAssembly();

            // Gets all the files that marked as Embedded resourse.
            foreach (var feedName in assembly.GetManifestResourceNames())
            {
                string content;
                using (var stream = assembly.GetManifestResourceStream(feedName))
                using (var reader = new StreamReader(stream))
                {
                    content = reader.ReadToEnd();
                }

                // TODO: Add dependency injection to this service
                var feedDataReader = new RaceDataReaderService(Providers);

                foreach (var horse in feedDataReader.GetHorseList(content))
                {
                    Console.WriteLine($"Name: {horse.Name}, Price: {horse.Price}");
                }
            }

            Console.Read();
        }
    }
}
