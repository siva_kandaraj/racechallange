﻿using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RaceChallange.Models.Requests;
using RaceChallange.Models.Responses;

namespace RaceChallange.Providers
{
    public class JsonFeedReader : IDataReadProvider
    {
        public List<Horse> GetHorseList(string content)
        {
            var d = JsonConvert.DeserializeObject<RootObject>(content);
            var marketParticipants = d.RawData.Markets.Select(_ => _.Selections.Select(p => new Horse
            {
                Name = p.Tags.name,
                Price = p.Price
            }));

            return marketParticipants.SelectMany(horse => horse).OrderBy(_ => _.Price).ToList();
        }

        public bool CanRead(string content)
        {
            try
            {
                JToken.Parse(content);
                return true;
            }
            catch (JsonReaderException ex)
            {
                // log debug error
                return false;
            }
        }
    }
}
