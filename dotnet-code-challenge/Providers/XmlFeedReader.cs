﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using dotnet_code_challenge;
using RaceChallange.Models.Responses;

namespace RaceChallange.Providers
{
    public class XmlFeedReader : IDataReadProvider
    {
        private double GetParticipantPrice(string participantId, IEnumerable<XElement> prices)
        {
            var priceElement = prices.First(p => p.Attribute("number")?.Value == participantId);

            if (priceElement != null) return double.Parse(priceElement.Attribute("Price")?.Value);

            return 0;
        }

        public List<Horse> GetHorseList(string content)
        {
            var doc = XDocument.Parse(content);
            var aa = doc.Descendants("meeting").Descendants("races").Descendants("race");

            var prices = aa.Elements("prices").Single().Descendants("price").Descendants("horses").Descendants("horse").ToList();

            var horses = aa.Elements("horses").Single().Elements("horse");

            var output = new List<Horse>();

            foreach (var horse in horses)
            {
                output.Add(new Horse
                {
                    Name = horse.Attribute("name")?.Value,
                    Price = GetParticipantPrice(horse.Elements("number").Single().Value, prices)
                });
            }

            return output.OrderBy(_ => _.Price).ToList();
        }

        public bool CanRead(string content)
        {
            try
            {
                var xDocument = XDocument.Parse(content);
            }
            catch (Exception e)
            {
                // log the debug error
                return false;
            }

            return true;
        }
    }
}
