﻿using System.Collections.Generic;

using RaceChallange.Models.Responses;

namespace dotnet_code_challenge
{
    public interface IDataReadProvider
    {
        List<Horse> GetHorseList(string content);

        bool CanRead(string content);
    }
}
