﻿namespace RaceChallange.Models.Responses
{
    public class Horse
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
