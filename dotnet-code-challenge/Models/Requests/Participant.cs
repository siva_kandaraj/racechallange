﻿namespace RaceChallange.Models.Requests
{
    public class Participant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Tags4 Tags { get; set; }
    }
}
