﻿namespace RaceChallange.Models.Requests
{
    public class Tags4
    {
        public string Weight { get; set; }
        public string Drawn { get; set; }
        public string Jockey { get; set; }
        public string Number { get; set; }
        public string Trainer { get; set; }
    }
}
