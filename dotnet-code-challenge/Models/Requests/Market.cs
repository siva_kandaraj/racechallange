﻿using System.Collections.Generic;

namespace RaceChallange.Models.Requests
{
    public class Market
    {
        public string Id { get; set; }
        public List<Selection> Selections { get; set; }
        public Tags3 Tags { get; set; }
    }
}
