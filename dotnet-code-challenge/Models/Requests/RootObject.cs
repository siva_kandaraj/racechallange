﻿using System;

namespace RaceChallange.Models.Requests
{
    public class RootObject
    {
        public string FixtureId { get; set; }
        public DateTime Timestamp { get; set; }
        public RawData RawData { get; set; }
    }
}
