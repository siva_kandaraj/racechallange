﻿namespace RaceChallange.Models.Requests
{
    public class Selection
    {
        public string Id { get; set; }
        public double Price { get; set; }
        public Tags2 Tags { get; set; }
    }
}
