﻿using System.IO;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using RaceChallange.Providers;
using Xunit;

namespace RaceChallange.Test.Providers
{
    public class XmlFeedReaderTest
    {
        [Fact]
        public void Give_Xml_When_Valid_Then_Should_Return_ListOfHorses()
        {
            var resourceName = "RaceChallange.Test.TestData.Caulfield_Race1.xml";
            string content;

            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                content = reader.ReadToEnd();
            }

            var xmlFeedReader = new XmlFeedReader();
            var canRead = xmlFeedReader.CanRead(content);
            canRead.Should().Be(true);

            var data = xmlFeedReader.GetHorseList(content);
            data.Should().HaveCount(2);
            data.FirstOrDefault()?.Name.Should().BeEquivalentTo("Advancing");
        }
    }
}
