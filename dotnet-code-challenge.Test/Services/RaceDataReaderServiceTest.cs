﻿using System.Collections.Generic;
using dotnet_code_challenge;
using FluentAssertions;
using NSubstitute;
using RaceChallange.Models.Responses;
using RaceChallange.Services;
using Xunit;

namespace RaceChallange.Test.Services
{
    public class RaceDataReaderServiceTest
    {
        private RaceDataReaderService _raceDataReader;
        private readonly IDataReadProvider _fakeReader = Substitute.For<IDataReadProvider>();

        public RaceDataReaderServiceTest()
        {
            _fakeReader.CanRead(Arg.Any<string>()).Returns(true);
            _fakeReader.GetHorseList(Arg.Any<string>()).Returns(new List<Horse> { new Horse() });
        }

        [Fact]
        public void Given_Content_When_Valid_Then_ReturnsListOfHorses()
        {
            IEnumerable<IDataReadProvider> providers = new List<IDataReadProvider>
            {
                _fakeReader
            };

            _raceDataReader = new RaceDataReaderService(providers);
            var horseList = _raceDataReader.GetHorseList("XML or Json content");

            horseList.Should().NotBeNull();
            horseList.Count.Should().Be(1);
        }
    }
}
